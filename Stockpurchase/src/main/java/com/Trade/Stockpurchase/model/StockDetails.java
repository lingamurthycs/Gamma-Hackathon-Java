package com.Trade.Stockpurchase.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

public class StockDetails {

	@NotNull
	private Integer stock_id;
	
	private String stock_name;
	private Float current_stock_price;
	private String currency_type;
	
	@NotNull
	private Integer no_of_shares;
	
	private Float total_share_price;

	public Integer getStock_id() {
		return stock_id;
	}

	public void setStock_id(Integer stock_id) {
		this.stock_id = stock_id;
	}

	public String getStock_name() {
		return stock_name;
	}

	public void setStock_name(String stock_name) {
		this.stock_name = stock_name;
	}

	public Float getCurrent_stock_price() {
		return current_stock_price;
	}

	public void setCurrent_stock_price(Float current_stock_price) {
		this.current_stock_price = current_stock_price;
	}

	public String getCurrency_type() {
		return currency_type;
	}

	public void setCurrency_type(String currency_type) {
		this.currency_type = currency_type;
	}

	public Integer getNo_of_shares() {
		return no_of_shares;
	}

	public void setNo_of_shares(Integer no_of_shares) {
		this.no_of_shares = no_of_shares;
	}

	public Float getTotal_share_price() {
		return total_share_price;
	}

	public void setTotal_share_price(Float total_share_price) {
		this.total_share_price = total_share_price;
	}

}
