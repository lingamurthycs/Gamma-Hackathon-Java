package com.Trade.Stockpurchase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Trade.Stockpurchase.Util.StockConstants;
import com.Trade.Stockpurchase.dao.StockPurchaseDaoI;
import com.Trade.Stockpurchase.model.StockDetails;

@Service
public class StockDaoService {

	@Autowired
	StockPurchaseDaoI stockPurchaseDaoI;
	

	public StockDetails getStockDetails(Integer stock_id, Integer qty) {
		StockDetails stockDetails = null;
		try {
			stockDetails = stockPurchaseDaoI.getStockDetails(stock_id, qty);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stockDetails;
	}

	public Boolean saveStockDetails(StockDetails stockDetails) {
		boolean updated = false;
		try {
			updated = stockPurchaseDaoI.saveStockDetails(stockDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updated;
	}
	
	public void setSharePriceDetails(StockDetails stockDetails, Integer qty) {
		
		float brokerage_fee = 0f;
		float total_share_price =  stockDetails.getCurrent_stock_price() * qty;
		if (qty != null && qty < StockConstants.SHARE_QTY_UPPPER_LIMIT) {
			brokerage_fee = ((total_share_price * StockConstants.STOCK_BROKERAGE_PERCENT_FOR_LT_500)/100);
		} else if (qty != null && qty >= StockConstants.SHARE_QTY_UPPPER_LIMIT) {
			brokerage_fee = ((total_share_price * StockConstants.STOCK_BROKERAGE_PERCENT_FOR_GT_EQ_500)/100);
		}
		total_share_price = total_share_price+brokerage_fee;
		stockDetails.setTotal_share_price(total_share_price);
		stockDetails.setNo_of_shares(qty);
		
	}

	public void validateUserInput(Integer valueOf, Integer valueOf2) {
		
	}

}
