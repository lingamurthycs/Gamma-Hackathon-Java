package com.Trade.Stockpurchase.Util;

public class StockConstants {
	
	public static final Float STOCK_BROKERAGE_PERCENT_FOR_LT_500 = 0.10f;
	public static final Float STOCK_BROKERAGE_PERCENT_FOR_GT_EQ_500 = 0.15f;
	
	public static final Integer SHARE_QTY_UPPPER_LIMIT = 500;
	
	public static final String GET_STOCK_DETAILS = "/gamma/stockdetails/{stock_id}/qty/{qty}";
	
	public static final String POST_STOCK_DETAILS = "/gamma/savestockdetails";
	
	public static final String SQL_GET_STOCK_DETAILS = "select * from GammaBank.Stock where Stock_Id = ?";
	public static final String SQL_SAVE_STOCK_DETAILS = "insert into GammaBank.Stock_Details(Stock_Id, CurrencyType, Number_Of_Shares, Total_Share_Price) values(?,?,?,?)";
	



}
