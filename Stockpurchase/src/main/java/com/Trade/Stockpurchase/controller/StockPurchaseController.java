package com.Trade.Stockpurchase.controller;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Trade.Stockpurchase.Util.StockConstants;
import com.Trade.Stockpurchase.model.StockDetails;
import com.Trade.Stockpurchase.service.StockDaoService;

@RestController
@CrossOrigin("*")
public class StockPurchaseController {
	
	@Inject
	StockDaoService stockDaoService;
	
	@GetMapping(StockConstants.GET_STOCK_DETAILS)
	public ResponseEntity<StockDetails> getStockDetails(@PathVariable(value = "stock_id",required = true) Integer stock_id ,@PathVariable(value = "qty",required = true) Integer qty ) {

		StockDetails stockDetails = stockDaoService.getStockDetails(stock_id,qty);
		
		stockDaoService.setSharePriceDetails(stockDetails,qty);

		return new ResponseEntity<>(stockDetails, HttpStatus.OK);

	}
	
	@PostMapping(StockConstants.POST_STOCK_DETAILS)
	public ResponseEntity<StockDetails> saveStockDetails(@RequestBody StockDetails stockDetails) {

		StockDetails stockDetailsDBObj = stockDaoService.getStockDetails(stockDetails.getStock_id(),stockDetails.getNo_of_shares());
		
		stockDaoService.setSharePriceDetails(stockDetailsDBObj,stockDetails.getNo_of_shares());
		
		stockDaoService.saveStockDetails(stockDetailsDBObj);

		return new ResponseEntity<>(stockDetailsDBObj, HttpStatus.OK);

	}
	

}
