package com.Trade.Stockpurchase.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.Trade.Stockpurchase.Util.StockConstants;
import com.Trade.Stockpurchase.model.StockDetails;

@Component
public class StockPurchaseDaoImpl implements StockPurchaseDaoI  {

	JdbcTemplate jdbcTemplate;

	
	@Autowired
	public StockPurchaseDaoImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public StockDetails getStockDetails(Integer stock_id, Integer qty) throws Exception {
		return (StockDetails) jdbcTemplate.queryForObject(StockConstants.SQL_GET_STOCK_DETAILS, new Object[] { stock_id }, new StockDetailsMapper());
	}
	
	@Override
	public boolean saveStockDetails(StockDetails stockDetails) throws Exception {
		return jdbcTemplate.update(StockConstants.SQL_SAVE_STOCK_DETAILS, stockDetails.getStock_id(), stockDetails.getCurrency_type(), stockDetails.getNo_of_shares(),
				stockDetails.getTotal_share_price()) > 0;		
	}
	
	
}
