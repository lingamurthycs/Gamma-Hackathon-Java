package com.Trade.Stockpurchase.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.Trade.Stockpurchase.model.StockDetails;

public class StockDetailsMapper implements RowMapper {

	@Override
	public Object mapRow(ResultSet resultSet, int arg1) throws SQLException {
		
		
		
		StockDetails stockDetails = new StockDetails();
		stockDetails.setStock_name(resultSet.getString("Stock_Name"));
		stockDetails.setStock_id(resultSet.getInt("Stock_Id"));
		stockDetails.setCurrent_stock_price(resultSet.getFloat("Current_Stock_Price"));
		stockDetails.setCurrency_type("doller");
		
	
		return stockDetails;
	}

}
