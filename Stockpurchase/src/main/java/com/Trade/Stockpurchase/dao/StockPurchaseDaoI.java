package com.Trade.Stockpurchase.dao;

import com.Trade.Stockpurchase.model.StockDetails;

public interface StockPurchaseDaoI {
	StockDetails getStockDetails(Integer stock_id, Integer qty) throws Exception;
	
	boolean saveStockDetails(StockDetails stockDetails) throws Exception;

	

}
